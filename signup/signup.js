
function signInUser() {


  fetch("../data/database.json")
    .then((res) => res.json())
    .then((data) => {
      const email = document.getElementById('email').value;
      const password = document.getElementById('password').value;
      let newemail = Object.keys(data).reduce((acc, curr) => {
        if (curr == email) {
          acc = curr;
        }
        return acc;
      }, "");
      let newpassword = Object.values(data).reduce((acc, curr) => {
        if (curr == password) {
          acc = curr;
        }
        return acc;
      }, "");
      if (email === '' || email === null) {
        document.getElementById('message').innerText = `Email is required`;
      }

      else if (password === '' || password === null) {

        document.getElementById('message').innerText = 'Password is required';
      }
      else if (password.length <= 10 && password != null && password.value != '') {
        document.getElementById('message').innerText = 'Password must be longer than 10 characters';
      }
      else if (password.length >= 15) {
        document.getElementById('message').innerText = 'Password must be lesser than 15 characters';
      }

      else if (password === 'password') {
        document.getElementById('message').innerText = 'Password cannot be password';
      }
      else if (password.match(/[a-z]/g) === null) {
        document.getElementById('message').innerText = 'password must be contain at least one lowercase letter';
      }
      else if (password.match(/[A-Z]/g) === null) {
        document.getElementById('message').innerText = 'password must be contain at least one uppercase letter';
      }
      else if (password.match(/[0-9]/g) === null) {
        document.getElementById('message').innerText = 'password must be contain at least one numeric digit';
      }
      else if (newemail == email && newpassword == password) {

        document.getElementById('message').innerText = 'Email already exists';

      }
      else if (newemail != email) {
        sendData();

      }

    })
}


function sendData() {

  const data = { email: document.getElementById('email').value, password: document.getElementById('password').value }
  fetch("http://localhost:5000/signup", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    body: JSON.stringify(data)
  });

}